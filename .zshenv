#set -ex
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export ZDOTDIR=${XDG_CONFIG_HOME}/zsh
export ZSH_COMPDUMP=${XDG_CACHE_HOME}/zsh/zcompdump
export EMACSDIR=${XDG_CONFIG_HOME}/emacs
export DOOMDIR=${XDG_CONFIG_HOME}/doom.d
PATH=:$PATH:${XDG_CONFIG_HOME}/doom-emacs/bin
PATH=:$PATH:$XDG_DATA_HOME/junest/bin
PATH=:$PATH:~/.cabal/bin
PATH=:$PATH:~/.ghcup/bin
PATH=:$PATH:~/.local/bin
export PATH=$PATH
export HISTFILE="${XDG_DATA_HOME}/zsh_history"
export HISTSIZE=10000
export JUNEST_HOME=/opt/junest
export ANTIGEN_HS_HOME=${ZDOTDIR}/antigen-hs
export ANTIGEN_HS_OUT=${XDG_DATA_HOME}/antigen.hs/
export TERM=xterm-256color
export TERMINAL=termonad
export GOPATH=${XDG_CONFIG_HOME}/go
export GOCACHE=${XDG_CACHE_HOME}/go-build
export ZSHZ_DATA=${XDG_DATA_HOME}/zsh/zshzdb
export $(dbus-launch)
export QT_QPA_PLATFORMTHEME=qt5ct
unset QT_STYLE_OVERRIDE
export EDITOR="emacsclient -c -t"
export READER="zathura"
export VISUAL="emacs"
export EMACS_SOCKET_NAME="$XDG_RUNTIME_DIR/emacs/server"
export VIDEO="mpv"
export MANPAGER="sh -c 'col -bx | bat --paging=always -l man -p'"
export IMAGE="vimiv"
export COLORTERM="truecolor"
export OPENER="mimeo"
export PAGER="less"
#source ~/.zsh/lficons
export XMONAD_CONFIG_HOME="$XDG_CONFIG_HOME/xmonad"
export XMONAD_DATA_DIR="$XDG_DATA_HOME/xmonad"
export XMONAD_CACHE_DIR="$XDG_CACHE_HOME/xmonad"
export SBCL_HOME=/usr/lib64/sbcl/
#stuff for river
export XKB_DEFAULT_OPTIONS="ctrl:nocaps"
export XKB_DEFAULT_LAYOUT="it(us)"
export XDG_CURRENT_DESKTOP=Unity
LS_COLORS="di=1;37:ex=4;31:*.mp3=1;32;41"
export LC_ALL="en_US.UTF-8"
