alias pacman-sdepclean="sudo id && sudo pacman -Qdtq | sudo pacman -Rs -"
alias fzf="sk"
alias r="rm -rvf"
alias cp="cp -rv"
alias vim="nvim"
alias jobs="jobs -p"
#alias sxiv="devour sxiv"
#alias zathura="devour zathura"
alias ls="lsd -F --color=auto"
alias grep="rg"
alias md='mkdir'
alias ...='../../../'
alias emacs='emacsclient -c -t'
alias s6-rc-user="s6-rc -l /tmp/${USER}/s6-rc"
alias batplain="bat --style=plain"
alias cat="bat"
alias catplain="bat --style=plain"
alias termbin="nc termbin.com 9999"
