autoload -U compinit && compinit -u  #autoload compinit
compinit -d $XDG_DATA_HOME/zcompdump
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search #load the up and down wiget to navigate  history
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
zle -N edit-command-line
zle -N deer #load deer
#sourcing stuff
source ${ZDOTDIR}/aliases.sh #source aliases
source ${ZDOTDIR}/deer #source deer
source ${ZDOTDIR}/keybinds.zsh #source personal keybindings
source ${ZDOTDIR}/functions #source deer
source ${ZDOTDIR}/antigen-hs/init.zsh #plugin manager's file
source ${ZDOTDIR}/ls_colors #ls colors
source ${XDG_CONFIG_HOME}/broot/launcher/bash/br
source ${HOME}/.config/broot/launcher/bash/br
source /usr/share/skim/key-bindings.zsh #source skim's key-bindings
source /usr/share/skim/completion.zsh #source skim's completions

set -o emacs
#have to put this here instead of .zshenv because else it won't work
export AUTO_NOTIFY_IGNORE=("junest" "iwctl" "zathura" "su" "lfcd" "n" "tg" "man" "less" "vim" "emacs" "bat" "doas vim" "htop"  "watch" "artix-chroot")
#adding colors to tab completions
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
#config for fzf-tab
zstyle ':fzf-tab:*' fzf-command sk 
#eval "$(starship init zsh)" #starship prompt
export PS1=" %?%B ﬦ %n @ %~ >>=%b " # prompt

