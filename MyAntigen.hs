  {-# LANGUAGE OverloadedStrings #-}
module MyAntigen where

import Antigen (
                -- Rudimentary imports
                AntigenConfig (..)
              , defaultConfig
              , bundle
              , antigen
                -- If you want to source a bit trickier plugins
              , ZshPlugin (..)
              , antigenSourcingStrategy
              , filePathsSourcingStrategy
              )

bundles =
  [ --bundle "Tarrasch/zsh-functional"
   bundle "Tarrasch/zsh-bd"
  , bundle "zsh-users/zsh-autosuggestions"
  , bundle "zdharma/fast-syntax-highlighting"
  , bundle "MichaelAquilina/zsh-auto-notify"
  --, bundle "zpm-zsh/colorize"
  , bundle "Aloxaf/fzf-tab"
  , bundle "andrewferrier/fzf-z"
  , bundle "agkozak/zsh-z"
    , bundle "mafredri/zsh-async"
  , bundle "ChrisPenner/copy-pasta"
  , bundle "commiyou/zsh-editing-workbench"
  --, bundle "Valodim/zsh-capture-completion"
  --, bundle "formula-spectre/f-shortcuts"
  --, bundle "changyuheng/zsh-interactive-cd"
  , (bundle "mdumitru/fancy-ctrl-z") { sourcingStrategy = antigenSourcingStrategy}
  --, (bundle "stanislas/allergen") {sourcingStrategy = antigenSourcingStrategy}
  --- this is possibly useless ^

  -- If you use a plugin that doesn't have a *.plugin.zsh file. You can set a
  -- more liberal sourcing strategy.
  --
  -- , (bundle "some/stupid-plugin") { sourcingStrategy = antigenSourcingStrategy }

  -- If you use a plugin that has sub-plugins. You can specify that as well
  --
  -- NOTE: If you want to use oh-my-zsh for real (please don't), you still need
  -- to set the $ZSH env var manually.
  -- , (bundle "robbyrussell/oh-my-zsh")
  --    { sourcingLocations = [ "plugins/wd"
  --                          , "plugins/colorize"] }
  --  , (bundle "belak/zsh-utils")
  --      { sourcingLocations = [
  --                              "editor"
  --                            , "history"
  --                            , "prompt"
  --                            , "utility"
  --                            , "completion"
  --                            ]
  --      }
      --am I doing this right?
  -- Sourcing a list of files
  -- , (bundle "alfredodeza/zsh-plugins")
  --    { sourcingStrategy = filePathsSourcingStrategy
  --                          [ "vi/zle_vi_visual.zsh"
  --                          , "pytest/pytest.plugin.zsh"
  --                          ] }
    , (bundle "ohmyzsh/ohmyzsh")
        { sourcingStrategy = filePathsSourcingStrategy
                              [
                                "lib/history.zsh"
   --                           , "plugins/fancy-ctrl-z/fancy-ctrl-z.plugin.zsh"
                              ]
        }
  -- Alternatively, this way will give you the same result
  -- , (bundle "alfredodeza/zsh-plugins")
  --    { sourcingStrategy = antigenSourcingStrategy
  --    , sourcingLocations = [ "vi"
  --                          , "pytest"
  --                          ] }

  -- vvv    Add your plugins here    vvv
  ]

config = defaultConfig { plugins = bundles }

main :: IO ()
main = antigen config
